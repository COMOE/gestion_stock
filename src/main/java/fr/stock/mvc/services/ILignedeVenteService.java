package fr.stock.mvc.services;

import java.util.List;

import fr.stock.mvc.entities.LignedeVente;

public interface ILignedeVenteService {
	public LignedeVente save( LignedeVente  entity );
	public LignedeVente update( LignedeVente  entity );
	public List<LignedeVente> selectAll();
	public LignedeVente getById(Long id);
	public void remove(Long id);
	public List<LignedeVente> selectAll(String sortFiled,String sort);
	public LignedeVente findOneBy(String paramName,String paramValue);
	public LignedeVente findOneBy(String [] paramNames,Object [] paramValues);
	public int findCountBy(String paramName,String paramValue);

}
