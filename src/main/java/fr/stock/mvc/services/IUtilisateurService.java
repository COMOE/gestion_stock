package fr.stock.mvc.services;

import java.util.List;

import fr.stock.mvc.entities.Utilisateur;

public interface IUtilisateurService {
	public Utilisateur save( Utilisateur  entity );
	public Utilisateur update( Utilisateur  entity );
	public List<Utilisateur> selectAll();
	public Utilisateur getById(Long id);
	public void remove(Long id);
	public List<Utilisateur> selectAll(String sortFiled,String sort);
	public Utilisateur findOneBy(String paramName,String paramValue);
	public Utilisateur findOneBy(String [] paramNames,Object [] paramValues);
	public int findCountBy(String paramName,String paramValue);

}
