package fr.stock.mvc.services;

import java.util.List;

import fr.stock.mvc.entities.CommandeClient;

public interface ICommandeClientService {
	public CommandeClient save( CommandeClient  entity );
	public CommandeClient update( CommandeClient  entity );
	public List<CommandeClient> selectAll();
	public CommandeClient getById(Long id);
	public void remove(Long id);
	public List<CommandeClient> selectAll(String sortFiled,String sort);
	public CommandeClient findOneBy(String paramName,String paramValue);
	public CommandeClient findOneBy(String [] paramNames,Object [] paramValues);
	public int findCountBy(String paramName,String paramValue);
}
