package fr.stock.mvc.services;

import java.util.List;

import fr.stock.mvc.entities.Article;

public interface IArticleService {
	public Article save( Article  entity );
	public Article update( Article  entity );
	public List<Article> selectAll();
	public Article getById(Long id);
	public void remove(Long id);
	public List<Article> selectAll(String sortFiled,String sort);
	public Article findOneBy(String paramName,String paramValue);
	public Article findOneBy(String [] paramNames,Object [] paramValues);
	public int findCountBy(String paramName,String paramValue);
}
