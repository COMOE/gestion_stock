package fr.stock.mvc.services;

import java.util.List;

import fr.stock.mvc.entities.Client;

public interface IClientService {
	public Client save( Client  entity );
	public Client update( Client  entity );
	public List<Client> selectAll();
	public Client getById(Long id);
	public void remove(Long id);
	public List<Client> selectAll(String sortFiled,String sort);
	public Client findOneBy(String paramName,String paramValue);
	public Client findOneBy(String [] paramNames,Object [] paramValues);
	public int findCountBy(String paramName,String paramValue);
}
