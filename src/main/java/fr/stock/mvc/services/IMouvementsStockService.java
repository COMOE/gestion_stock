package fr.stock.mvc.services;

import java.util.List;

import fr.stock.mvc.entities.MouvementsStock;

public interface IMouvementsStockService {
	public MouvementsStock save( MouvementsStock  entity );
	public MouvementsStock update( MouvementsStock  entity );
	public List<MouvementsStock> selectAll();
	public MouvementsStock getById(Long id);
	public void remove(Long id);
	public List<MouvementsStock> selectAll(String sortFiled,String sort);
	public MouvementsStock findOneBy(String paramName,String paramValue);
	public MouvementsStock findOneBy(String [] paramNames,Object [] paramValues);
	public int findCountBy(String paramName,String paramValue);

}
