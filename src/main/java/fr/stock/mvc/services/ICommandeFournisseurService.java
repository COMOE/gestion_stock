package fr.stock.mvc.services;

import java.util.List;

import fr.stock.mvc.entities.CommandeFournisseur;

public interface ICommandeFournisseurService {
	public CommandeFournisseur save( CommandeFournisseur  entity );
	public CommandeFournisseur update( CommandeFournisseur  entity );
	public List<CommandeFournisseur> selectAll();
	public CommandeFournisseur getById(Long id);
	public void remove(Long id);
	public List<CommandeFournisseur> selectAll(String sortFiled,String sort);
	public CommandeFournisseur findOneBy(String paramName,String paramValue);
	public CommandeFournisseur findOneBy(String [] paramNames,Object [] paramValues);
	public int findCountBy(String paramName,String paramValue);
}
