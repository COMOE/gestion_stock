package fr.stock.mvc.services;

import java.util.List;

import fr.stock.mvc.entities.LigneCommandeClient;

public interface ILigneCommandeClientService {
	public LigneCommandeClient save( LigneCommandeClient  entity );
	public LigneCommandeClient update( LigneCommandeClient  entity );
	public List<LigneCommandeClient> selectAll();
	public LigneCommandeClient getById(Long id);
	public void remove(Long id);
	public List<LigneCommandeClient> selectAll(String sortFiled,String sort);
	public LigneCommandeClient findOneBy(String paramName,String paramValue);
	public LigneCommandeClient findOneBy(String [] paramNames,Object [] paramValues);
	public int findCountBy(String paramName,String paramValue);

}
