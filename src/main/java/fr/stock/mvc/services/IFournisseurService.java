package fr.stock.mvc.services;

import java.util.List;

import fr.stock.mvc.entities.Fournisseur;

public interface IFournisseurService {
	public Fournisseur save( Fournisseur  entity );
	public Fournisseur update( Fournisseur  entity );
	public List<Fournisseur> selectAll();
	public Fournisseur getById(Long id);
	public void remove(Long id);
	public List<Fournisseur> selectAll(String sortFiled,String sort);
	public Fournisseur findOneBy(String paramName,String paramValue);
	public Fournisseur findOneBy(String [] paramNames,Object [] paramValues);
	public int findCountBy(String paramName,String paramValue);
}
