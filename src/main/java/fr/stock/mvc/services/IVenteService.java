package fr.stock.mvc.services;

import java.util.List;

import fr.stock.mvc.entities.Vente;

public interface IVenteService {
	public Vente save( Vente  entity );
	public Vente update( Vente  entity );
	public List<Vente> selectAll();
	public Vente getById(Long id);
	public void remove(Long id);
	public List<Vente> selectAll(String sortFiled,String sort);
	public Vente findOneBy(String paramName,String paramValue);
	public Vente findOneBy(String [] paramNames,Object [] paramValues);
	public int findCountBy(String paramName,String paramValue);

}
