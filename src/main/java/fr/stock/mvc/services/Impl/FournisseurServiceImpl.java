package fr.stock.mvc.services.Impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import fr.stock.mvc.dao.IFournisseurDao;
import fr.stock.mvc.entities.Fournisseur;
import fr.stock.mvc.services.IFournisseurService;

@Transactional
public class FournisseurServiceImpl implements IFournisseurService{
	
	private IFournisseurDao dao;
	
	public void setDao(IFournisseurDao dao) {
		this.dao = dao;
	}


	public Fournisseur save(Fournisseur entity) {
		return dao.save(entity);
	}


	public Fournisseur update(Fournisseur entity) {
		return dao.update(entity);
	}


	public List<Fournisseur> selectAll() {
		return dao.selectAll();
	}


	public Fournisseur getById(Long id) {
		return dao.getById(id);
	}


	public void remove(Long id) {
		dao.remove(id);
		
	}


	public List<Fournisseur> selectAll(String sortFiled, String sort) {
		return dao.selectAll(sortFiled, sort);
	}


	public Fournisseur findOneBy(String paramName, String paramValue) {
		return dao.findOneBy(paramName, paramValue);
	}


	public Fournisseur findOneBy(String[] paramNames, Object[] paramValues) {
		return dao.findOneBy(paramNames, paramValues);
	}


	public int findCountBy(String paramName, String paramValue) {
		return dao.findCountBy(paramName, paramValue);
	}
	

}
