package fr.stock.mvc.services.Impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import fr.stock.mvc.dao.IClientDao;
import fr.stock.mvc.entities.Client;
import fr.stock.mvc.services.IClientService;

@Transactional
public class ClientServiceImpl implements IClientService{
	
	private IClientDao dao;
	
	public void setDao(IClientDao dao) {
		this.dao = dao;
	}


	public Client save(Client entity) {
		return dao.save(entity);
	}


	public Client update(Client entity) {
		return dao.update(entity);
	}


	public List<Client> selectAll() {
		return dao.selectAll();
	}


	public Client getById(Long id) {
		return dao.getById(id);
	}


	public void remove(Long id) {
		dao.remove(id);
		
	}


	public List<Client> selectAll(String sortFiled, String sort) {
		return dao.selectAll(sortFiled, sort);
	}


	public Client findOneBy(String paramName, String paramValue) {
		return dao.findOneBy(paramName, paramValue);
	}


	public Client findOneBy(String[] paramNames, Object[] paramValues) {
		return dao.findOneBy(paramNames, paramValues);
	}


	public int findCountBy(String paramName, String paramValue) {
		return dao.findCountBy(paramName, paramValue);
	}
	

}
