package fr.stock.mvc.services.Impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import fr.stock.mvc.dao.IVenteDao;
import fr.stock.mvc.entities.Vente;
import fr.stock.mvc.services.IVenteService;

@Transactional
public class VenteServiceImpl implements IVenteService{
	
	private IVenteDao dao;
	
	public void setDao(IVenteDao dao) {
		this.dao = dao;
	}


	public Vente save(Vente entity) {
		return dao.save(entity);
	}


	public Vente update(Vente entity) {
		return dao.update(entity);
	}


	public List<Vente> selectAll() {
		return dao.selectAll();
	}


	public Vente getById(Long id) {
		return dao.getById(id);
	}


	public void remove(Long id) {
		dao.remove(id);
		
	}


	public List<Vente> selectAll(String sortFiled, String sort) {
		return dao.selectAll(sortFiled, sort);
	}


	public Vente findOneBy(String paramName, String paramValue) {
		return dao.findOneBy(paramName, paramValue);
	}


	public Vente findOneBy(String[] paramNames, Object[] paramValues) {
		return dao.findOneBy(paramNames, paramValues);
	}


	public int findCountBy(String paramName, String paramValue) {
		return dao.findCountBy(paramName, paramValue);
	}
	

}
