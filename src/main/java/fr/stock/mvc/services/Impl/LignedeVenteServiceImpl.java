package fr.stock.mvc.services.Impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import fr.stock.mvc.dao.ILignedeVenteDao;
import fr.stock.mvc.entities.LignedeVente;
import fr.stock.mvc.services.ILignedeVenteService;

@Transactional
public class LignedeVenteServiceImpl implements ILignedeVenteService{
	
	private ILignedeVenteDao dao;
	
	public void setDao(ILignedeVenteDao dao) {
		this.dao = dao;
	}


	public LignedeVente save(LignedeVente entity) {
		return dao.save(entity);
	}


	public LignedeVente update(LignedeVente entity) {
		return dao.update(entity);
	}


	public List<LignedeVente> selectAll() {
		return dao.selectAll();
	}


	public LignedeVente getById(Long id) {
		return dao.getById(id);
	}


	public void remove(Long id) {
		dao.remove(id);
		
	}


	public List<LignedeVente> selectAll(String sortFiled, String sort) {
		return dao.selectAll(sortFiled, sort);
	}


	public LignedeVente findOneBy(String paramName, String paramValue) {
		return dao.findOneBy(paramName, paramValue);
	}


	public LignedeVente findOneBy(String[] paramNames, Object[] paramValues) {
		return dao.findOneBy(paramNames, paramValues);
	}


	public int findCountBy(String paramName, String paramValue) {
		return dao.findCountBy(paramName, paramValue);
	}
	

}
