package fr.stock.mvc.services.Impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import fr.stock.mvc.dao.IUtilisateurDao;
import fr.stock.mvc.entities.Utilisateur;
import fr.stock.mvc.services.IUtilisateurService;

@Transactional
public class UtilisateurServiceImpl implements IUtilisateurService{
	
	private IUtilisateurDao dao;
	
	public void setDao(IUtilisateurDao dao) {
		this.dao = dao;
	}


	public Utilisateur save(Utilisateur entity) {
		return dao.save(entity);
	}


	public Utilisateur update(Utilisateur entity) {
		return dao.update(entity);
	}


	public List<Utilisateur> selectAll() {
		return dao.selectAll();
	}


	public Utilisateur getById(Long id) {
		return dao.getById(id);
	}


	public void remove(Long id) {
		dao.remove(id);
		
	}


	public List<Utilisateur> selectAll(String sortFiled, String sort) {
		return dao.selectAll(sortFiled, sort);
	}


	public Utilisateur findOneBy(String paramName, String paramValue) {
		return dao.findOneBy(paramName, paramValue);
	}


	public Utilisateur findOneBy(String[] paramNames, Object[] paramValues) {
		return dao.findOneBy(paramNames, paramValues);
	}


	public int findCountBy(String paramName, String paramValue) {
		return dao.findCountBy(paramName, paramValue);
	}
	

}
