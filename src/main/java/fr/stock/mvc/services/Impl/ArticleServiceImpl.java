package fr.stock.mvc.services.Impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import fr.stock.mvc.dao.IArticleDao;
import fr.stock.mvc.entities.Article;
import fr.stock.mvc.services.IArticleService;

@Transactional
public class ArticleServiceImpl implements IArticleService{
	
	private IArticleDao dao;
	
	public void setDao(IArticleDao dao) {
		this.dao = dao;
	}


	public Article save(Article entity) {
		return dao.save(entity);
	}


	public Article update(Article entity) {
		return dao.update(entity);
	}


	public List<Article> selectAll() {
		return dao.selectAll();
	}


	public Article getById(Long id) {
		return dao.getById(id);
	}


	public void remove(Long id) {
		dao.remove(id);
		
	}


	public List<Article> selectAll(String sortFiled, String sort) {
		return dao.selectAll(sortFiled, sort);
	}


	public Article findOneBy(String paramName, String paramValue) {
		return dao.findOneBy(paramName, paramValue);
	}


	public Article findOneBy(String[] paramNames, Object[] paramValues) {
		return dao.findOneBy(paramNames, paramValues);
	}


	public int findCountBy(String paramName, String paramValue) {
		return dao.findCountBy(paramName, paramValue);
	}
	

}
