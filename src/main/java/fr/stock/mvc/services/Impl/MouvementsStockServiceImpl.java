package fr.stock.mvc.services.Impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import fr.stock.mvc.dao.IMouvementsStockDao;
import fr.stock.mvc.entities.MouvementsStock;
import fr.stock.mvc.services.IMouvementsStockService;

@Transactional
public class MouvementsStockServiceImpl implements IMouvementsStockService{
	
	private IMouvementsStockDao dao;
	
	public void setDao(IMouvementsStockDao dao) {
		this.dao = dao;
	}


	public MouvementsStock save(MouvementsStock entity) {
		return dao.save(entity);
	}


	public MouvementsStock update(MouvementsStock entity) {
		return dao.update(entity);
	}


	public List<MouvementsStock> selectAll() {
		return dao.selectAll();
	}


	public MouvementsStock getById(Long id) {
		return dao.getById(id);
	}


	public void remove(Long id) {
		dao.remove(id);
		
	}


	public List<MouvementsStock> selectAll(String sortFiled, String sort) {
		return dao.selectAll(sortFiled, sort);
	}


	public MouvementsStock findOneBy(String paramName, String paramValue) {
		return dao.findOneBy(paramName, paramValue);
	}


	public MouvementsStock findOneBy(String[] paramNames, Object[] paramValues) {
		return dao.findOneBy(paramNames, paramValues);
	}


	public int findCountBy(String paramName, String paramValue) {
		return dao.findCountBy(paramName, paramValue);
	}
	

}
