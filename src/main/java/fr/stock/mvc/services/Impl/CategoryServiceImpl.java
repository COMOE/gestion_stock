package fr.stock.mvc.services.Impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import fr.stock.mvc.dao.ICategoryDao;
import fr.stock.mvc.entities.Category;
import fr.stock.mvc.services.ICategoryService;

@Transactional
public class CategoryServiceImpl implements ICategoryService{
	
	private ICategoryDao dao;
	
	public void setDao(ICategoryDao dao) {
		this.dao = dao;
	}


	public Category save(Category entity) {
		return dao.save(entity);
	}


	public Category update(Category entity) {
		return dao.update(entity);
	}


	public List<Category> selectAll() {
		return dao.selectAll();
	}


	public Category getById(Long id) {
		return dao.getById(id);
	}


	public void remove(Long id) {
		dao.remove(id);
		
	}


	public List<Category> selectAll(String sortFiled, String sort) {
		return dao.selectAll(sortFiled, sort);
	}


	public Category findOneBy(String paramName, String paramValue) {
		return dao.findOneBy(paramName, paramValue);
	}


	public Category findOneBy(String[] paramNames, Object[] paramValues) {
		return dao.findOneBy(paramNames, paramValues);
	}


	public int findCountBy(String paramName, String paramValue) {
		return dao.findCountBy(paramName, paramValue);
	}
	

}
