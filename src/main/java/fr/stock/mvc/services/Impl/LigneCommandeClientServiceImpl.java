package fr.stock.mvc.services.Impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import fr.stock.mvc.dao.ILigneCommandeClientDao;
import fr.stock.mvc.entities.LigneCommandeClient;
import fr.stock.mvc.services.ILigneCommandeClientService;

@Transactional
public class LigneCommandeClientServiceImpl implements ILigneCommandeClientService{
	
	private ILigneCommandeClientDao dao;
	
	public void setDao(ILigneCommandeClientDao dao) {
		this.dao = dao;
	}


	public LigneCommandeClient save(LigneCommandeClient entity) {
		return dao.save(entity);
	}


	public LigneCommandeClient update(LigneCommandeClient entity) {
		return dao.update(entity);
	}


	public List<LigneCommandeClient> selectAll() {
		return dao.selectAll();
	}


	public LigneCommandeClient getById(Long id) {
		return dao.getById(id);
	}


	public void remove(Long id) {
		dao.remove(id);
		
	}


	public List<LigneCommandeClient> selectAll(String sortFiled, String sort) {
		return dao.selectAll(sortFiled, sort);
	}


	public LigneCommandeClient findOneBy(String paramName, String paramValue) {
		return dao.findOneBy(paramName, paramValue);
	}


	public LigneCommandeClient findOneBy(String[] paramNames, Object[] paramValues) {
		return dao.findOneBy(paramNames, paramValues);
	}


	public int findCountBy(String paramName, String paramValue) {
		return dao.findCountBy(paramName, paramValue);
	}
	

}
