package fr.stock.mvc.services.Impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import fr.stock.mvc.dao.ICommandeClientDao;
import fr.stock.mvc.entities.CommandeClient;
import fr.stock.mvc.services.ICommandeClientService;

@Transactional
public class CommandeClientServiceImpl implements ICommandeClientService{
	
	private ICommandeClientDao dao;
	
	public void setDao(ICommandeClientDao dao) {
		this.dao = dao;
	}


	public CommandeClient save(CommandeClient entity) {
		return dao.save(entity);
	}


	public CommandeClient update(CommandeClient entity) {
		return dao.update(entity);
	}


	public List<CommandeClient> selectAll() {
		return dao.selectAll();
	}


	public CommandeClient getById(Long id) {
		return dao.getById(id);
	}


	public void remove(Long id) {
		dao.remove(id);
		
	}


	public List<CommandeClient> selectAll(String sortFiled, String sort) {
		return dao.selectAll(sortFiled, sort);
	}


	public CommandeClient findOneBy(String paramName, String paramValue) {
		return dao.findOneBy(paramName, paramValue);
	}


	public CommandeClient findOneBy(String[] paramNames, Object[] paramValues) {
		return dao.findOneBy(paramNames, paramValues);
	}


	public int findCountBy(String paramName, String paramValue) {
		return dao.findCountBy(paramName, paramValue);
	}
	

}
