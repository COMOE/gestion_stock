package fr.stock.mvc.services.Impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import fr.stock.mvc.dao.ICommandeFournisseurDao;
import fr.stock.mvc.entities.CommandeFournisseur;
import fr.stock.mvc.services.ICommandeFournisseurService;

@Transactional
public class CommandeFournisseurServiceImpl implements ICommandeFournisseurService{
	
	private ICommandeFournisseurDao dao;
	
	public void setDao(ICommandeFournisseurDao dao) {
		this.dao = dao;
	}


	public CommandeFournisseur save(CommandeFournisseur entity) {
		return dao.save(entity);
	}


	public CommandeFournisseur update(CommandeFournisseur entity) {
		return dao.update(entity);
	}


	public List<CommandeFournisseur> selectAll() {
		return dao.selectAll();
	}


	public CommandeFournisseur getById(Long id) {
		return dao.getById(id);
	}


	public void remove(Long id) {
		dao.remove(id);
		
	}


	public List<CommandeFournisseur> selectAll(String sortFiled, String sort) {
		return dao.selectAll(sortFiled, sort);
	}


	public CommandeFournisseur findOneBy(String paramName, String paramValue) {
		return dao.findOneBy(paramName, paramValue);
	}


	public CommandeFournisseur findOneBy(String[] paramNames, Object[] paramValues) {
		return dao.findOneBy(paramNames, paramValues);
	}


	public int findCountBy(String paramName, String paramValue) {
		return dao.findCountBy(paramName, paramValue);
	}
	

}
