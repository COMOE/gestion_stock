package fr.stock.mvc.services.Impl;

import java.io.InputStream;

import org.springframework.transaction.annotation.Transactional;

import fr.stock.mvc.dao.IFlickrDao;
import fr.stock.mvc.services.IFlickrService;

@Transactional
public class FlickrServiceImpl implements IFlickrService {

    private IFlickrDao dao;
	
	public void setDao(IFlickrDao dao) {
		this.dao = dao;
	}
	@Override
	public String savePhoto(InputStream photo, String title) throws Exception {
		
		return dao.savePhoto(photo, title);
	}

}
