package fr.stock.mvc.services;

import java.util.List;

import fr.stock.mvc.entities.LigneCommandeFournisseur;

public interface ILigneCommandeFournisseurService {
	public LigneCommandeFournisseur save( LigneCommandeFournisseur  entity );
	public LigneCommandeFournisseur update( LigneCommandeFournisseur  entity );
	public List<LigneCommandeFournisseur> selectAll();
	public LigneCommandeFournisseur getById(Long id);
	public void remove(Long id);
	public List<LigneCommandeFournisseur> selectAll(String sortFiled,String sort);
	public LigneCommandeFournisseur findOneBy(String paramName,String paramValue);
	public LigneCommandeFournisseur findOneBy(String [] paramNames,Object [] paramValues);
	public int findCountBy(String paramName,String paramValue);

}
