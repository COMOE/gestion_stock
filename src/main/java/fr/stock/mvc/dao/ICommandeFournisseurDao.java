package fr.stock.mvc.dao;

import fr.stock.mvc.entities.CommandeFournisseur;

public interface ICommandeFournisseurDao extends IGenericDao<CommandeFournisseur>{

}
