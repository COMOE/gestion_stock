package fr.stock.mvc.dao;

import fr.stock.mvc.entities.MouvementsStock;

public interface IMouvementsStockDao extends IGenericDao<MouvementsStock> {

}
