package fr.stock.mvc.dao;

import fr.stock.mvc.entities.CommandeClient;

public interface ICommandeClientDao extends IGenericDao<CommandeClient>{

}
