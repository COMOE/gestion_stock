package fr.stock.mvc.dao.impl;

import java.io.InputStream;

import javax.swing.JOptionPane;

import org.scribe.model.Token;
import org.scribe.model.Verifier;

import com.flickr4java.flickr.Flickr;
import com.flickr4java.flickr.FlickrException;
import com.flickr4java.flickr.REST;
import com.flickr4java.flickr.RequestContext;
import com.flickr4java.flickr.auth.Auth;
import com.flickr4java.flickr.auth.AuthInterface;
import com.flickr4java.flickr.auth.Permission;
import com.flickr4java.flickr.uploader.UploadMetaData;

import fr.stock.mvc.dao.IFlickrDao;

public class FlickrDaoImpl implements IFlickrDao{

	private Flickr flickr;
	private UploadMetaData uploadMetaData= new UploadMetaData();
	private String apiKey="c8c8d4a747aa979f339e8ab995042935";
	private String sharedSecret="2d869ebb0ea750ac";
	
	public void connect(){
		
		flickr=new Flickr(apiKey,sharedSecret,new REST());
		Auth auth=new Auth();
		auth.setPermission(Permission.READ);
		auth.setToken("72157694875041882-294dd4828d0a52d1");
		auth.setTokenSecret("fcace517902d538e");
		RequestContext requestContext = RequestContext.getRequestContext();
		requestContext.setAuth(auth);
		flickr.setAuth(auth);
	}
	
	@Override
	public String savePhoto(InputStream photo, String title) throws Exception {
		connect();
		uploadMetaData.setTitle(title);
		String photoId=flickr.getUploader().upload(photo, uploadMetaData);
		return flickr.getPhotosInterface().getPhoto(photoId).getMedium640Url();
	}
	
	public void auth(){
		flickr =new Flickr(apiKey,sharedSecret,new REST());
		AuthInterface authInterface =flickr.getAuthInterface();
		Token token =authInterface.getRequestToken();
		System.out.println("token "+token);
		
		String url=authInterface.getAuthorizationUrl(token, Permission.DELETE);
		System.out.println("Follow this URL to authorise youtseft on Flickr");
		System.out.println(url);
		System.out.println("Paste in the token it gives you :");
		System.out.println(">>");
		
		String tokenKey=JOptionPane.showInputDialog(null);
		Token requestToken=authInterface.getAccessToken(token,new Verifier(tokenKey));
		System.out.println("Authentification success");
		
		Auth auth =null;
		try {
			auth=authInterface.checkToken(requestToken);
			
		}catch(FlickrException e){
			e.printStackTrace();
		}
		
		System.out.println("Token: "+requestToken.getToken());
		System.out.println("Secret: "+requestToken.getSecret());
		System.out.println("nsid: "+auth.getUser().getId());
		System.out.println("Realname: "+auth.getUser().getRealName());
		System.out.println("Username: "+auth.getUser().getUsername());
		System.out.println("Permission: "+auth.getPermission().getType());

	}

}
