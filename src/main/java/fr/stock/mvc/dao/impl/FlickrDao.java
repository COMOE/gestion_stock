package fr.stock.mvc.dao.impl;

import java.io.InputStream;
import org.scribe.model.Token;
import org.scribe.model.Verifier;

import com.flickr4java.flickr.Flickr;
import com.flickr4java.flickr.FlickrException;
import com.flickr4java.flickr.REST;
import com.flickr4java.flickr.RequestContext;
import com.flickr4java.flickr.auth.Auth;
import com.flickr4java.flickr.auth.AuthInterface;
import com.flickr4java.flickr.auth.Permission;
import com.flickr4java.flickr.uploader.UploadMetaData;

import fr.stock.mvc.dao.IFlickrDao;

public class FlickrDao implements IFlickrDao{

	private Flickr flickr;
	private UploadMetaData uploadMetaData= new UploadMetaData();
	private String apiKey="c8c8d4a747aa979f339e8ab995042935";
	private String sharedSecret="2d869ebb0ea750ac";
	
	public void connect(){
		
		flickr=new Flickr(apiKey,sharedSecret,new REST());
		Auth auth=new Auth();
		auth.setPermission(Permission.READ);
		auth.setToken("7215");
		//auth.setTokenSecret(tokenSecret);
		RequestContext requestContext = RequestContext.getRequestContext();
		requestContext.setAuth(auth);
		flickr.setAuth(auth);
	}
	
	@Override
	public String savePhoto(InputStream photo, String title) throws Exception {
		connect();
		uploadMetaData.setTitle(title);
		String photoId=flickr.getUploader().upload(photo, uploadMetaData);
		return flickr.getPhotosInterface().getPhoto(photoId).getMedium640Url();
	}
	
	public void auth(){
		flickr =new Flickr(apiKey,sharedSecret,new REST());
		AuthInterface authInterface =flickr.getAuthInterface();
		Token token =authInterface.getRequestToken();
		System.out.println("token "+token);
		
		String url=authInterface.getAuthorizationUrl(token, Permission.DELETE);
		System.out.println("Follow this URL to authorise youtseft on Flickr");
	}

}
