package fr.stock.mvc.dao;

import fr.stock.mvc.entities.Article;

public interface IArticleDao extends IGenericDao<Article>{

}
