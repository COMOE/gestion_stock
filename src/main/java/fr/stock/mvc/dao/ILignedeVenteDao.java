package fr.stock.mvc.dao;

import fr.stock.mvc.entities.LignedeVente;

public interface ILignedeVenteDao extends IGenericDao<LignedeVente> {

}
