package fr.stock.mvc.dao;

import fr.stock.mvc.entities.LigneCommandeFournisseur;

public interface ILigneCommandeFournisseurDao extends IGenericDao<LigneCommandeFournisseur>{

}
