package fr.stock.mvc.dao;

import fr.stock.mvc.entities.LigneCommandeClient;

public interface ILigneCommandeClientDao extends IGenericDao<LigneCommandeClient>{

}
