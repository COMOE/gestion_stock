package fr.stock.mvc.dao;

import fr.stock.mvc.entities.Fournisseur;

public interface IFournisseurDao extends IGenericDao<Fournisseur>{

}
