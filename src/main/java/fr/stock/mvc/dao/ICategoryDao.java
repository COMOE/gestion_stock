package fr.stock.mvc.dao;

import fr.stock.mvc.entities.Category;

public interface ICategoryDao extends IGenericDao<Category> {

}
