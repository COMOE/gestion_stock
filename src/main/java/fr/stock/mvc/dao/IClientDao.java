package fr.stock.mvc.dao;

import fr.stock.mvc.entities.Client;

public interface IClientDao extends IGenericDao<Client>{

}
