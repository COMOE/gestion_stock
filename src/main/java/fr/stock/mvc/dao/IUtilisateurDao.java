package fr.stock.mvc.dao;

import fr.stock.mvc.entities.Utilisateur;

public interface IUtilisateurDao extends IGenericDao<Utilisateur>{

}
