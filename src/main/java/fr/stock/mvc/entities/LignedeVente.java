package fr.stock.mvc.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="LignedeVente")
public class LignedeVente implements Serializable {
	@Id
	@GeneratedValue
	private Long idLignedeVente;
	
	@ManyToOne
	@JoinColumn(name="idArticle")
	private Article article;
	
	@ManyToOne
	@JoinColumn(name="idVente")
	private Vente vente;

	public Long getIdLignedeVente() {
		return idLignedeVente;
	}

	public void setIdLignedeVente(Long id) {
		this.idLignedeVente = id;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	public Vente getVente() {
		return vente;
	}

	public void setVente(Vente vente) {
		this.vente = vente;
	}
	
	
	
}
