package fr.stock.mvc.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="mouvementsStock")
public class MouvementsStock implements Serializable{
	private static final int ENTREE=1;
	private static final int SORTIE=2;
	@Id
	@GeneratedValue
	private Long idMouvementsStock;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateMouvementsStock;
	
	private BigDecimal quantite;
	private int typeMouvementsStock;
	
	@ManyToOne
	@JoinColumn(name="idArticle")
	private Article article;
	
	
	
	public Long getIdMouvementsStock() {
		return idMouvementsStock;
	}

	public void setIdMouvementsStock(Long id) {
		this.idMouvementsStock = id;
	}

	public Date getDateMouvementsStock() {
		return dateMouvementsStock;
	}

	public void setDateMouvementsStock(Date dateMouvementsStock) {
		this.dateMouvementsStock = dateMouvementsStock;
	}

	public BigDecimal getQuantite() {
		return quantite;
	}

	public void setQuantite(BigDecimal quantite) {
		this.quantite = quantite;
	}

	public int getTypeMouvementsStock() {
		return typeMouvementsStock;
	}

	public void setTypeMouvementsStock(int typMouvementsStock) {
		this.typeMouvementsStock = typMouvementsStock;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

    
	
	
}
