package fr.stock.mvc.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="article")
public class Article implements Serializable {

	@Id
	@GeneratedValue
	private Long idArticle;
	private String codeArticle;
	private String designation;
	private BigDecimal prixUnitaireHT;
	private BigDecimal tauxTva;
	private BigDecimal prixUnitaireTTC;
	private String Photo;
	
	@ManyToOne
	@JoinColumn(name="idCategory")
	private Category category;
	
	@OneToMany(mappedBy="article")
	private List<LigneCommandeClient> ligneCommandeClient;
	
	@OneToMany(mappedBy="vente")
	private List<LignedeVente> lignedeVentes;
	
	@OneToMany(mappedBy="article")
	private List<MouvementsStock> mouvementsStock;
	
	
	public Article() {
		// TODO Auto-generated constructor stub
	}

	public Long getIdArticle() {
		return idArticle;
	}

	public void setIdArticle(Long idArticle) {
		this.idArticle = idArticle;
	}

	public String getCodeArticle() {
		return codeArticle;
	}

	public void setCodeArticle(String codeArticle) {
		this.codeArticle = codeArticle;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public BigDecimal getPrixUnitaireHT() {
		return prixUnitaireHT;
	}

	public void setPrixUnitaireHT(BigDecimal prixUnitaireHT) {
		this.prixUnitaireHT = prixUnitaireHT;
	}

	public BigDecimal getTauxTva() {
		return tauxTva;
	}

	public void setTauxTva(BigDecimal tauxTva) {
		this.tauxTva = tauxTva;
	}

	public BigDecimal getPrixUnitaireTTC() {
		return prixUnitaireTTC;
	}

	public void setPrixUnitaireTTC(BigDecimal prixUnitaireTTC) {
		this.prixUnitaireTTC = prixUnitaireTTC;
	}

	public String getPhoto() {
		return Photo;
	}

	public void setPhoto(String photo) {
		Photo = photo;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public List<LigneCommandeClient> getLigneCommandeClient() {
		return ligneCommandeClient;
	}

	public void setLigneCommandeClient(List<LigneCommandeClient> ligneCommandeClient) {
		this.ligneCommandeClient = ligneCommandeClient;
	}

	public List<LignedeVente> getLignedeVentes() {
		return lignedeVentes;
	}

	public void setLignedeVentes(List<LignedeVente> lignedeVentes) {
		this.lignedeVentes = lignedeVentes;
	}

	public List<MouvementsStock> getMouvementsStock() {
		return mouvementsStock;
	}

	public void setMouvementsStock(List<MouvementsStock> mouvementsStock) {
		this.mouvementsStock = mouvementsStock;
	}
	
	
	
	
	
}
